package com.plume.code.common.constrant;

public class GeneratorConstant {

    public interface Type {
        String service = "service";
        String serviceImpl = "serviceImpl";
        String dto = "dto";
        String mybatisPlusEnt = "mybatisPlusEnt";
        String mainVue = "mainVue";
        String elementTableVue = "elementTableVue";
    }
}
