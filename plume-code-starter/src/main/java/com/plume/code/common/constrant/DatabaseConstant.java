package com.plume.code.common.constrant;

public class DatabaseConstant {
    public interface PkStrategy {
        Integer NONE = 0;
        Integer AUTO_INCREMENT = 1;

        //TBD
        Integer UUID = 2;
    }
}
