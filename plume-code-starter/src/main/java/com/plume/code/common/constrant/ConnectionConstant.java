package com.plume.code.common.constrant;

/**
 * @author yinyansheng
 * setting constant
 */
public class ConnectionConstant {

    public interface DatabaseType {
        String H2 = "h2";
        String Mysql = "mysql";
    }
}
