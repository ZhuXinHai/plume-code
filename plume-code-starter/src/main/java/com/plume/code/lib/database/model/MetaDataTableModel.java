package com.plume.code.lib.database.model;

import lombok.Data;

@Data
public class MetaDataTableModel {
    private String TABLE_NAME;
    private String REMARKS;
}
