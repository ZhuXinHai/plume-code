package com.plume.code.lib.database.model;

import lombok.Data;

@Data
public class ClassModel {
    private String tableName;
    private String name;
    private String comment;
}
