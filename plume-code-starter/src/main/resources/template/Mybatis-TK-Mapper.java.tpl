package ${packageName};

import ${basePackageName}.mapper.entity.${ClassName}${setting.entPostfix};
import tk.mybatis.mapper.common.Mapper;
/**
 * @description: ${comment}
 * @author: ${author}
 * @date: ${createTime}
 **/
public interface ${ClassName}Mapper extends Mapper<${ClassName}${setting.entPostfix}>  {
}
